# Changelog

## Release v1.7.1
 * Fixed bug if RSS feed contains links that do not have type metadata [3c8e431]

## Release v1.7
 * Gotta pull Python3.5 image [35688d5]
 * Adding Python3.5 to develop pipeline [7a3a95f]
 * Fixing pipelines bug [56d1a9b]
 * Adding python3.7 to testing suite [ff92d5f]
 * Only testing specific versions of python on develop branch. [07b074b]
 * Added Python3.8 to tox testing [3ceefab]
 * Updated requirements [87a9051]
 * Updated unit/integration tests. [12f3c6e]
 * Added logging (INFO and DEBUG) to blacksap [082a321]

## Release v1.6.2
 * Spacing is important. [e9584bd]
 * Changing around logging. [c6a80e8]
 * Updating bitbucket-pipelines with PyPy3 tests [46d79a8]
 * Updated Requirements [8ec6f0b]
 * Adding Python 3.5 back into tests. [7d65ebd]
 * Bumping Version [80f34df]

## Release v1.6.1
 * Updated README with new settings. [ab0ca59]
 * Switching from md5 to sha1 for RSS hashing. [13d1554]
 * Updated requirements. [adcc269]
 * Updated URL for logo in README.md [75fc172]
 * Trying out new multi-step pipeline. [4308a37]

## Release v1.6
 * Added ability to enable/disable feeds via config file. [ff6be35]
 * Updated requirements and tox testing [54d375b]

## Release v1.5.1
 * Displaying whether rules are enabled or not per feed. [76ff144]

## Release v1.5
 * Adding ability to apply regular expression rules to torrent files to be downloaded from an RSS feed. [e9cb531]
 * Updated debug log messages for RegEx searching. [e831eb8]
 * Mocking integration tests using requests_mock. [4db90c0]

## Release v1.4.3
 * Fixed bug when reversing feed and limiting number of entries processed. [883d162]

## Release v1.4.2
 * Starting another attempt at CICD… [a083f36]
 * Cosmetic Fixes. [e6c3273]
 * Ensuring .pypyrc is removed at the end of deploy.bash [47145d5]
 * Updating urls checked in blacksap. [59e27af]
 * Configuring defaults for blacksap app settings [aee011e]
 * Updated requirements and version info [01eb58d]

## Release v1.4.1
 * Missing requirement of PySocks for SOCKS5 proxy support! [a6d8b33]
 * Apparently Bitbucket Pipelines doesn’t support multiple steps yet [7e93a22]

## Release v1.4
 * Added ability to configure app internal settings (e.g. threads) in cfg file. [43b6288]
 * Updating README.md [2c47c7f]
 * Adding ability to set proxies and/or disable IPv6 in blacksap config file. [e0c5453]
 * Implementing Python logging. [a35baf0]
 * Added some example settings. [2a35383]

## Release v1.3
 * Blacksap is now multi-threaded.  Simplified commands and did lots of re-writes. [166b914]
 * Unit/Integration Tests are working again [7397fd9]
 * Started using Bitbucket Pipelines with Python3.6 [8342e38]

## Release v1.2.7
 * Updated user-agent. [a41e0d4]
 * Automating testing across Python 2.7, 3.5, and 3.6 [2ded455]
 * Fixing minor things… [7498503]
 * Removing reset command from blacksap. [0e74cd0]
 * Removing integration test for clear cache.  Added pypy to list of environments to test. [ded7458]

## Release v1.2.6
 * Attempting to clean up status print statements. [0e66fcd]
 * Updates stopwatch class name [261632c]
 * Updated to work with extratorrent.cc [b1edd48]
 * Updated integration tests. [385e403]
 * Ensuring files end with .torrent. [621e84f]
 * Fixing filenames that have / in them. [72a1fde]

## Release v1.2.5
 * Changed - Updated user-agent for requests
 * Fixed - Minor bugs

## Release v1.2.4
 * Fixed - Better catching of SSL issues - [Issue #6](https://bitbucket.org/isaiah1112/blacksap/issues/6/odd-ssl-issue)

## Release v1.2.3
 * Fixed - Ensuring that output directory is indeed a directory

## Release v1.2.2
 * Fixed - Made printouts from run process more descriptive

## Release v1.2.1
 * Fixed - Bug with printing http error code when torrent file doesn't download

## Release v1.2
 * Fixed - Multiple torrents with the same name are only downloaded once
 * Fixed - Bug when url to torrent file returns 404 - [Issue #5](https://bitbucket.org/isaiah1112/blacksap/issues/5/)
 * Improved - Now using requests method to download torrent files rather than subprocessing out to curl

## Release v1.1
 * Compatibility with Python 2.7.x and 3.5.x - Yay!
 * Fixed - perfected untrack command
 * Fixed - https redirects when tracking feed - [Issue #3](https://bitbucket.org/isaiah1112/blacksap/issues/3/)
 * Added - cli short options
 * Added - now specify feeds to check by url when using run command - [Issue #4](https://bitbucket.org/isaiah1112/blacksap/issues/4/)
 * Added - error messages are now printed to stderr
 * Changed - verbose flag is now debug
 * Changed - moved to subprocess module for curl calls
 * Changed - output location is now under --output cli option

## Release v1.0.1
 * Fixed - Bug when untracking a url that isn't being tracked - [Issue #2](https://bitbucket.org/isaiah1112/blacksap/issues/2/)
 * New - Submitted blacksap to PyPi - [Issue #1](https://bitbucket.org/isaiah1112/blacksap/issues/1/)

## Release v1.0
 * Initial Release of blacksap
