#!/usr/bin/env python
# coding=utf-8
""" Unit and integration tests for blacksap.
"""
from __future__ import print_function
from click.testing import CliRunner
import blacksap
import os
import requests_mock
import shutil
import unittest
import warnings

__author__ = 'Jesse Almanrode (jesse@almanrode.com)'

blacksap.cfg_file = '/tmp/blacksap.cfg'


class TestFunctions(unittest.TestCase):

    def setUp(self):
        with open('tests/rss_data.xml') as f:
            self.rss_data = f.read()

    @requests_mock.Mocker()
    def test_get_torrent_file(self, m):
        """ Test whether or not we can download a url
        :return: True and False
        """
        m.get(requests_mock.ANY, text=self.rss_data)
        try:
            blacksap.download_torrent_file('not_a_valid_url', '/tmp/', 'blacksap.test')
        except blacksap.BSError:
            pass
        result = blacksap.download_torrent_file('http://example.com', '/tmp/', 'blacksap.test.torrent')
        self.assertTrue(result)
        try:
            os.remove('/tmp/blacksap.test.torrent')
        except Exception:
            pass
        pass

    @requests_mock.Mocker()
    def test_download_rss_feed(self, m):
        """ Test downloading RSS Feed
        return namedtuple
        """
        try:
            blacksap.download_rss_feed('not_a_url')
        except blacksap.BSError:
            pass
        m.get('http://example.com', text='No RSS')
        try:
            blacksap.download_rss_feed('http://bitbucket.org/')
        except blacksap.BSError:
            pass
        m.get('http://example.com/rss.xml', text=self.rss_data)
        result = blacksap.download_rss_feed('http://example.com/rss.xml')
        self.assertIsInstance(result, blacksap.RSSFeed)
        pass


class TestCli(unittest.TestCase):

    def setUp(self):
        """ Set stuff up!
        """
        self.runner = CliRunner()
        with open('tests/rss_data.xml') as f:
            self.rss_data = f.read()

    def test_help(self):
        """ Make sure there aren't any parse errors
        :return: exit_code == 0
        """
        result = self.runner.invoke(blacksap.cli, ['--help'])
        self.assertEqual(result.exit_code, 0)
        pass

    def test_tracking(self):
        """ Test tracking command
        :return: exit_code == 0 and 'feeds tracked' in output
        """
        result = self.runner.invoke(blacksap.cli, ['tracking'])
        self.assertEqual(result.exit_code, 0)
        self.assertIn('feeds tracked', result.output)
        pass

    @requests_mock.Mocker()
    def test_track_valid(self, m):
        """ Test track command for a valid URL/Feed
        :return: exit_code == 0
        """
        m.get('http://example.com/rss.xml', text=self.rss_data)
        result = self.runner.invoke(blacksap.cli, ['track', 'http://example.com/rss.xml'])
        self.assertEqual(result.exit_code, 0)
        pass

    @requests_mock.Mocker()
    def test_track_invalid(self, m):
        """ Test track command for invalid URL/Feed
        :return: exit_code == 1
        """
        m.get('http://example.com', text='No RSS')
        result = self.runner.invoke(blacksap.cli, ['track', 'http://example.com'])
        self.assertNotEqual(result.exit_code, 0)
        pass

    @requests_mock.Mocker()
    def test_untrack_valid(self, m):
        """ Test untrack command for valid URL/Feed
        :return: exit_code == 0
        """
        m.get('http://example.com/rss.xml', text=self.rss_data)
        result = self.runner.invoke(blacksap.cli, ['untrack', 'http://example.com/rss.xml'])
        self.assertEqual(result.exit_code, 0)
        pass

    @requests_mock.Mocker()
    def test_untrack_invalid(self, m):
        """ Test untrack command for invalid URL/Feed
        :return: exit_code == 0
        """
        m.get('http://example.com', text='No RSS')
        result = self.runner.invoke(blacksap.cli, ['untrack', 'http://example.com'])
        self.assertEqual(result.exit_code, 0)
        pass

    @requests_mock.Mocker()
    def test_run(self, m):
        """ Test run command
        :return:
        """
        m.get(requests_mock.ANY, text=self.rss_data)
        self.test_track_valid()
        os.mkdir('/tmp/blacksap_test/')
        result = self.runner.invoke(blacksap.cli, ['run', '-o', '/tmp/blacksap_test/'])
        shutil.rmtree('/tmp/blacksap_test/')
        self.assertEqual(result.exit_code, 0)
        self.assertIn('feeds checked in', result.output)
        pass


if __name__ == '__main__':
    with warnings.catch_warnings(record=True):
        unittest.main()
        os.remove('/tmp/blacksap.cfg')
